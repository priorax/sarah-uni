<?php

       echo '<hr /><h1>PDO Example</h1><hr />';

       $street = '80 Swanston St';

	$db_dsn  = "mysql:host=DATABASE_HOST;dbname=DATABASE_NAME";
	$db_user = "DATABASE_USERNAME";
	$db_pass = "DATABASE_PASSWORD";

	$db = new PDO($db_dsn, $db_user, $db_pass);

	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$query = "SELECT * FROM property WHERE street = :street";

	$pdo = $db->prepare($query);

	$pdo->bindParam('street',   $street);

	$pdo->execute();

        $results = $pdo->fetchAll(PDO::FETCH_ASSOC);

        print_r($results);

?>
